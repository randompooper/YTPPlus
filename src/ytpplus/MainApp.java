/* Copyright 2019 randompooper
 * This file is part of YTPPlus.
 *
 * YTPPlus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * YTPPlus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YTPPlus.  If not, see <https://www.gnu.org/licenses/>.
 */
package ytpplus;
import java.util.Map;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
public class MainApp {
    public static void configd() {
        ytp.setLazySwitch(true);
        ytp.setLazySeek(true);
        ytp.setMaxClips(100);
        ytp.setClipSoundChance(50.0);

        ytp.setEffect("Squidward", 1);
        ytp.setEffect("Dance", 2);
        //ytp.setMinDuration(0.8);
        //ytp.setMaxDuration(1.1);
    }
    public static void config0() {
        ytp.setMinDuration(0.8);
        ytp.setMaxDuration(2.0);
        ytp.setMaxClips(80);
        ytp.setEffectChance(50);
        ytp.setTransitionClipChance(1);
        ytp.setLazySwitch(true);
        ytp.setLazySwitchInterrupt(20);
        ytp.setLazySwitchChance(10);
        ytp.setLazySwitchMaxClips(-1);
        ytp.setLazySeek(true);
        ytp.setLazySeekChance(10);
        ytp.setLazySeekInterrupt(40.0);
        ytp.setLazySeekNearby(true);
        ytp.setLazySeekNearbyMin(0.0);
        ytp.setLazySeekNearbyMax(8.0);
        ytp.setLazySeekFromStart(false);

        ytp.setEffect("RandomSoundMute", 5);
        ytp.setEffect("RandomSound", 5);
        ytp.setEffect("Reverse", 10);
        ytp.setEffect("SlowDown", 5);
        ytp.setEffect("SpeedUp", 5);
        ytp.setEffect("Squidward", 1);
        ytp.setEffect("Vibrato", 10);
        ytp.setEffect("Chorus", 10);
        ytp.setEffect("Dance", 2);
        ytp.setEffect("HighPitch", 5);
        ytp.setEffect("LowPitch", 5);
        ytp.setEffect("Mirror", 10);
        ytp.setEffect("Scale", 5);
        ytp.setEffect("GMajor", 10);
        ytp.setEffect("OverlayClip", 10);
        ytp.setClipSoundChance(50.0);
    }
    public static void configr() {
        ytp.setMinDuration(tools.randomDouble(0.2, 1.5));
        ytp.setMaxDuration(tools.randomDouble(Math.max(ytp.getMinDuration(), 0.8), 4.0));
        ytp.setMaxClips(tools.randomInt(50, 100));
        ytp.setEffectChance(tools.randomInt(20, 80));
        ytp.setTransitionClipChance(tools.randomInt(0, 10));
        ytp.setLazySwitch(true);
        ytp.setLazySwitchInterrupt(tools.randomInt(0, 80));
        ytp.setLazySwitchChance(tools.randomInt(0, 80));
        ytp.setLazySwitchMaxClips(-1);
        ytp.setLazySeek(true);
        ytp.setLazySeekChance(tools.randomInt(0, 80));
        ytp.setLazySeekInterrupt(tools.randomInt(0, 80));
        ytp.setLazySeekNearby(true);
        ytp.setLazySeekNearbyMin(tools.randomDouble(0.0, 20.0));
        ytp.setLazySeekNearbyMax(tools.randomDouble(Math.max(ytp.getLazySeekNearbyMin(), 0.0), 20.0));
        ytp.setLazySeekFromStart(tools.probability(50));

        ytp.setEffect("RandomSoundMute", tools.randomInt(20));
        ytp.setEffect("RandomSound", tools.randomInt(20));
        ytp.setEffect("Reverse", tools.randomInt(20));
        ytp.setEffect("SlowDown", tools.randomInt(20));
        ytp.setEffect("SpeedUp", tools.randomInt(20));
        ytp.setEffect("Squidward", tools.randomInt(3));
        ytp.setEffect("Vibrato", tools.randomInt(20));
        ytp.setEffect("Chorus", tools.randomInt(20));
        ytp.setEffect("Dance", tools.randomInt(5));
        ytp.setEffect("HighPitch", tools.randomInt(20));
        ytp.setEffect("LowPitch", tools.randomInt(20));
        ytp.setEffect("Mirror", tools.randomInt(20));
        ytp.setEffect("Scale", tools.randomInt(20));
        ytp.setEffect("GMajor", tools.randomInt(20));
        ytp.setEffect("OverlayClip", tools.randomInt(20));
        ytp.setClipSoundChance(tools.randomInt(100));
    }
    public static void config() {
        configr();
        ytp.setReconvertEffected(false);
        ytp.setConcatMethod(2);
    }
    public static void sources_list() {
        try {
            String path;
            BufferedReader br = new BufferedReader(new FileReader(new File("active.txt")));
            while ((path = br.readLine()) != null)
                if (!ytp.addSource(path))
                    System.out.println("Failed to import: " + path);

        } catch (Exception ex) {
            System.out.println("Had problems importing sources list");
        }
    }
    public static void sources_rand(int count) {
        ArrayList<String> sources = new ArrayList<String>();
        try {
            String path;
            BufferedReader br = new BufferedReader(new FileReader(new File("active.txt")));
            while ((path = br.readLine()) != null)
                sources.add(path);
        } catch (Exception ex) {
            System.out.println("Had problems importing sources list");
        }
        count = Math.min(count, sources.size());
        for (int i = 0; i < count; ++i) {
            int ri = tools.randomInt(sources.size());
            ytp.addSource(sources.get(ri));
            sources.remove(ri);
        }
    }
    public static void sources() {
        sources_list();
    }
    public static void dump_script(YTPGenerator.Script[] scr) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File("last_script.txt")));
        bw.write("" + scr.length);
        bw.newLine();
        for (YTPGenerator.Script s : scr) {
            /*System.err.println("File: " + s.file);
            System.err.println("Interval: " + (s.interval == null ? "none" : s.interval.getFirst().toString() + " " + s.interval.getSecond().toString()));
            System.err.println("Effect: " + (s.applyEffect == null ? "none" : s.applyEffect.getName()));
            System.err.println();*/
            bw.write(s.file);
            bw.newLine();
            if (s.interval == null) {
                bw.write("none");
                bw.newLine();
            } else {
                bw.write(s.interval.getFirst().toString());
                bw.write(" ");
                bw.write(s.interval.getSecond().toString());
                bw.newLine();
            }
            if (s.applyEffect == null) {
                bw.write("none");
                bw.newLine();
            } else {
                bw.write(s.applyEffect.getName());
                bw.newLine();
            }
        }
        bw.close();
    }
    public static YTPGenerator.Script[] import_script() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(new File("last_script.txt")));
        /* OH YEAAH! IT IS UNSAFE! I KNOW! JUST DON'T BE DUMBASS AND DON'T PARSE UNTRUSTED SCRIPTS! */
        int count = Integer.parseInt(br.readLine());
        YTPGenerator.Script[] scr = new YTPGenerator.Script[count];
        ytp.configureEffects();
        for (int i = 0; i < count; ++i) {
            scr[i] = ytp.new Script();
            scr[i].file = br.readLine();
            String line = br.readLine();
            if (line.equals("none"))
                scr[i].interval = null;
            else {
                String[] intervals = line.split("\\s");
                scr[i].interval = new Pair<TimeStamp,TimeStamp>(new TimeStamp(intervals[0]), new TimeStamp(intervals[1]));
            }
            line = br.readLine();
            if (randomize) {
                scr[i].applyEffect = tools.getRandomEffect();
            } else if (line.equals("none"))
                scr[i].applyEffect = null;
            else {
                try {
                    scr[i].applyEffect = EffectsFactory.class.getMethod(line, String.class);
                } catch (Exception ex) {
                    System.out.println("Effect " + line + " not found!");
                }
            }
        }
        br.close();
        if (false && randomize) {
            YTPGenerator.Script[] scr2 = ytp.randomScript();
            for (int i = scr.length - 1; i >= 0; --i) {
                if (tools.probability(50))
                    scr[i] = scr2[tools.randomInt(scr2.length - 1)];
                else {
                    YTPGenerator.Script s = scr[i];
                    int ri = tools.randomInt(0, i);
                    scr[i] = scr[ri];
                    scr[ri] = s;
                }
            }
        }
        return scr;
    }
    public static void test() {
        if (true) return;
        System.exit(0);
    }
    public static void main(String[] args) {
        ytp = new YTPGenerator("output/job_" + System.currentTimeMillis() + ".mp4");
        tools = ytp.getTools();

        test();
        config();
        sources();
        ytp.setTemp("temp/job" + System.currentTimeMillis() + "/");
        new File(ytp.getTemp()).mkdir();

        ytp.setProgressCallback(ytp.new ProgressCallback() {
            @Override
            public void progress(double v) {
                System.out.println("Progress: " + (v * 100.0) + "%");
            }
            @Override
            public void done(String errors) {
                if (errors != null)
                    System.out.println(errors);
            }
        });
        YTPGenerator.Script[] scr;
        scr = ytp.randomScript();
        try {
            //scr = import_script();
            dump_script(scr);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Had problems dumping/importing script: " + ex);
            /* Return is required in this case since scr will be null */
            return;
        }
        ytp.go(scr);
    }
    private static YTPGenerator ytp;
    private static EffectsFactory tools;
    private static boolean randomize = false;
}
